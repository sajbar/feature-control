<?

class Feature
{

    private static $_features = [];

    /**
     * Function for checking if a specific feature is enabled or not
     * will return false if feature is unknown
     *
     * @param string $feature
     * @return bool
     */
    public static function isEnabled($feature)
    {
        //only parse if not done already
        if (empty(self::$_features)) {
            self::_parseFeatureList();
        }
        //the feature is unknown, better mark it as disabled
        if (!isset(self::$_features[$feature])) {
            return false;
        }
        return self::$_features[$feature];
    }

    /**
     *
     * Function for performing the actual parsing of the featurelist.
     * Based on enabled tag and version tag it will test if a given feature is enabled or not
     *
     * For sake of reading, even features that are disabled are included in the array, they could be left out
     * as unknown features always will marked as disabled
     */
    private static function _parseFeatureList()
    {

        $featureListFile = dirname(__FILE__) . "/feature-list.xml";

        //if feature-list not found, nothing to parse
        if (!is_file($featureListFile)) {
            return;
        }

        $versionFile = dirname(__FILE__) . "/version.txt";
        //if version not found, then don't continue
        if (!is_file($versionFile)) {
            return;
        }

        $version = file_get_contents($versionFile);

        $xml = simplexml_load_file($featureListFile);

        $nodes = $xml->xpath("/features/feature");
        foreach ($nodes as $node) {
            //get the name of the feature, make sure to get the actual value, not the simplexmlelement.
            $feature = (string)$node->name;

            //string comparison as xml values are string always
            if ("false" == $node->enabled) {
                //feature is hard disabled, no need to compare version
                self::$_features[$feature] = false;
                continue;
            }
            //get the version of the feature, make sure to get the actual value, not the simplexmlelement.
            $featureVersion = (string)$node->version;
            //check if the feature is enabled in this version of software
            if (version_compare($version, $featureVersion, ">=")) {
                self::$_features[$feature] = true;
            } else {
                self::$_features[$feature] = false;
            }
        }
    }
}