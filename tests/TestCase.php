<?php
use PHPUnit\Framework\TestCase;

require_once( '../Feature.php');
class FeatureBitTest extends TestCase
{

    public function testUndefinedFeatureReturnsFalse()
    {
        $expected = false;
        $actual = Feature::isEnabled('undefined');

        $this->assertEquals($expected,$actual, "Expected Feature to not be disabled, as it's unknown");
    }

    public function testFeatureDisabledReturnsFalse()
    {
        $expected = false;
        $actual = Feature::isEnabled('disabled');

        $this->assertEquals($expected,$actual, "Expected Feature to not be enabled, as it's disabled");
    }

    public function testFeatureEnabledReturnsTrue()
    {
        $expected = true;
        $actual = Feature::isEnabled('Enabled');

        $this->assertEquals($expected,$actual, "Expected Feature to be enabled, as it's enabled");
    }

    public function testFeatureEnabledBuildVersionToLowReturnsFalse()
    {
        $expected = false;
        $actual = Feature::isEnabled('toLowBuildVersion');

        $this->assertEquals($expected,$actual, "Expected Feature to be disabled, as it's enabled, but build version is to low");
    }

    public function testFeatureEnabledBuildVersionHigherReturnsTrue()
    {
        $expected = true;
        $actual = Feature::isEnabled('buildVersionHigher');

        $this->assertEquals($expected,$actual, "Expected Feature to be enabled, as it's enabled, and build version is higher");
    }

    public function testFeatureEnabledBuildVersionSameReturnsTrue()
    {
        $expected = true;
        $actual = Feature::isEnabled('buildVersionSame');

        $this->assertEquals($expected,$actual, "Expected Feature to be enabled, as it's enabled, and build version is the same");
    }


    public function testFeatureEnabledBuildVersionSameButDisabledReturnsFalse()
    {
        $expected = false;
        $actual = Feature::isEnabled('buildVersionSameButDisabled');

        $this->assertEquals($expected,$actual, "Expected Feature to be disabled, as it's disabled, even tough build version is the same");
    }

    public function testFeatureEnabledBuildVersionHigherButDisabledReturnsFalse()
    {
        $expected = false;
        $actual = Feature::isEnabled('buildVersionHigherButDisabled');

        $this->assertEquals($expected,$actual, "Expected Feature to be disabled, as it's disabled, even tough build version is higher");
    }

    public function testFeatureVersionMissingReturnsFalse()
    {
        $expected = false;
        $actual = Feature::isEnabled('versionMissing');

        $this->assertEquals($expected,$actual, "Expected Feature to be disabled, as version is missing");
    }
    public function testFeatureVersionEmptyReturnsFalse()
    {
        $expected = false;
        $actual = Feature::isEnabled('versionEmpty');

        $this->assertEquals($expected,$actual, "Expected Feature to be disabled, as version is empty");
    }
}
